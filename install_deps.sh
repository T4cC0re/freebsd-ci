#!/bin/sh

set -xe

install_deps () {
  echo "Installing git and LFS"
  sudo pkg install -y git git-lfs
}

install_runner () {
  echo "Installing GitLab runner"

  if ! id gitlab-runner; then
    echo "gitlab-runner user does not exist yet. Creating..."
    sudo pw group add -n gitlab-runner
    sudo pw user add -n gitlab-runner -g gitlab-runner -s /usr/local/bin/bash
    sudo pw group mod gitlab-runner -m gitlab-runner
    sudo mkdir /home/gitlab-runner
    sudo chown gitlab-runner:gitlab-runner /home/gitlab-runner
  else
      echo "gitlab-runner user exists. Skipping..."
  fi

  echo "Fetching latest gitlab-runner binary..."
  sudo fetch -o /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-freebsd-amd64
  sudo chmod +x /usr/local/bin/gitlab-runner

  echo "Setting up gitlab-runner system-files and -configuration"
  sudo touch /var/log/gitlab_runner.log
  sudo chown gitlab-runner:gitlab-runner /var/log/gitlab_runner.log

  echo "Setting up sudoers for gitlab-runner user"
  cat <<EOF | sudo tee /usr/local/etc/sudoers.d/gitlab-runner
Defaults:gitlab-runner !requiretty
gitlab-runner ALL=(ALL) NOPASSWD: /usr/local/bin/pot *
EOF
  sudo chmod 440 /usr/local/etc/sudoers.d/gitlab-runner

  sudo mkdir -p /usr/local/etc/rc.d
  sudo bash -c 'cat > /usr/local/etc/rc.d/gitlab_runner' << "EOF"
#!/bin/sh
# PROVIDE: gitlab_runner
# REQUIRE: DAEMON NETWORKING
# BEFORE:
# KEYWORD:

. /etc/rc.subr

name="gitlab_runner"
rcvar="gitlab_runner_enable"

user="gitlab-runner"
user_home="/home/gitlab-runner"
command="/usr/local/bin/gitlab-runner"
command_args="run"
pidfile="/var/run/${name}.pid"

start_cmd="gitlab_runner_start"

gitlab_runner_start()
{
    export USER=${user}
    export HOME=${user_home}
    if checkyesno ${rcvar}; then
        cd ${user_home}
        /usr/sbin/daemon -u ${user} -p ${pidfile} ${command} ${command_args} > /var/log/gitlab_runner.log 2>&1
    fi
}

load_rc_config $name
run_rc_command $1
EOF

  sudo chmod +x /usr/local/etc/rc.d/gitlab_runner
  sudo sysrc gitlab_runner_enable=yes
  echo "GitLab runner installed"
}

write_info () {
  echo "Writing ABI info"
  pkg -vv | sed 's/ //g;s/:/-/g;s/^/export /g' | grep ABI | sudo tee /usr/local/etc/gitlab-ci-env
  echo -n 'export INSTALL_DATE=' | sudo tee -a /usr/local/etc/gitlab-ci-env
  TZ=UTC date +"%Y-%m-%dT%H:%M:%SZ" | sudo tee -a /usr/local/etc/gitlab-ci-env
}

install_deps
install_runner
write_info
