# FreeBSD CI for GitLab CI

## Why? Can't I just install the [GitLab-Runner]()?

Yes, you can. Fundamentally this is doing the same. But it does *much* more.

1. It runs every job in FreeBSD jails
2. It allows you to run builds on different FreeBSD versions\*
3. Easy to maintain and reinstall
4. It runs on any\*\* Linux machine

Also I needed a CI pipeline, and just installing a runner was boring.

\* as long as you defined the version in the setup step and it is less than or
equal to the FreeBSD version of the VM/Host.  
\*\* qemu and KVM capability required.

## How?

Glad you asked! This is implemented via custom executors for GitLab CI.

You have a FreeBSD VM (running on KVM), which is the manager.  
When GitLab CI offers up a new job, that matches the defined labels, this executor
will create a new FreeBSD jail, run the job in it, and destroy it again.  
You can control the version of FreeBSD to run from within your `.gitlab-ci.yml`
(as long as you defined the version in the setup step and it is less than or equal
to the FreeBSD version of the VM).

For example the following `.gitlab-ci.yml` would start the job `hello world` in a FreeBSD 11.3 jail
```yaml
hello world:
  tags:
    - FreeBSD
  image: gitlab-ci-11.3
  script:
    - echo "hello FreeBSD 11.3"
```
TODO: More

## Installation

Quick start:

- Sets up a FreeBSD 12.1 VM with 12.1 and 11.3 jails

```bash
REG_TOKEN=<runner registration token> make setup
```

Sometimes the VM does not boot cleanly if it was already set up. In this case, run `make stop` and re-run the setup above.

### Requirements

 - GitLab.com or self-hosted GitLab 12.9+
 - QEMU and KVM
 - Runner registration token (group or project)
 - `packer`
 - `sshpass`
 - `qemu-system-x86_64`

TODO
