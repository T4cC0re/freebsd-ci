#!/bin/sh

set -e
set -x

sudo freebsd-update --not-running-from-cron fetch
sudo freebsd-update --not-running-from-cron install

sudo pkg install -y pot

sed -e 's/\(ttyv[^0].*getty.*\)on /\1off/' /etc/ttys | sudo tee /etc/ttys > /dev/null

sudo tee -a /etc/rc.conf <<EOF
sendmail_enable="NO"
sendmail_submit_enable="NO"
sendmail_outbound_enable="NO"
sendmail_msp_queue_enable="NO"
EOF

pkg -vv | sed 's/ //g;s/:/-/g;s/^/export /g' | grep ABI | sudo tee /usr/local/etc/gitlab-ci-env

echo 'POT_EXTIF=vtnet0' | sudo tee -a /usr/local/etc/pot/pot.conf
sudo pot init -v
