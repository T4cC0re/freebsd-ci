RAM ?= 2048
CORES ?= 4
ABI ?= 12
REG_TOKEN ?=

output/FreeBSD-$(ABI)-amd64.qcow2: FreeBSD-$(ABI)-amd64.json installer/ install_deps.sh
	rm -rf output
	packer build FreeBSD-$(ABI)-amd64.json

box-$(ABI).img: output/FreeBSD-$(ABI)-amd64.qcow2
	cp output/FreeBSD-$(ABI)-amd64.qcow2 box-$(ABI).img

run: box-$(ABI).img
	qemu-system-x86_64 -enable-kvm -cpu host -name GitLab-CI-FreeBSD-$(ABI) -m $(RAM) -smp $(CORES) -display none -vga none -drive file=box-$(ABI).img,if=virtio -nic user,model=virtio-net-pci,hostfwd=tcp::5555-:22 -device virtio-serial -serial stdio

.PHONY: rund
rund: box-$(ABI).pid
box-$(ABI).pid: | box-$(ABI).img
	qemu-system-x86_64 -daemonize -enable-kvm -cpu host -name GitLab-CI-FreeBSD-$(ABI) -m $(RAM) -smp $(CORES) -display none -vga none -drive file=box-$(ABI).img,if=virtio -nic user,model=virtio-net-pci,hostfwd=tcp::5555-:22 -device virtio-serial -serial file:box-$(ABI).log -pidfile box-$(ABI).pid

.PHONY: ssh
ssh: box-$(ABI).pid
	while ! ./ssh.sh exit; do sleep 5; done

.PHONY: setup
setup: box-$(ABI).pid ssh setup.sh
	REG_TOKEN=$(REG_TOKEN) ./setup.sh 11.3 12.1

ping: setup
	./ssh.sh sh run_in_jail.sh ping -c 1 1.1.1.1

.PHONY: stop
stop:
	./ssh.sh sudo -iu gitlab-runner gitlab-runner unregister --all-runners || true
	test -f box-$(ABI).pid && kill $(shell cat box-$(ABI).pid) || true
	rm -f box-$(ABI).pid
	rm -f box-$(ABI).img

clean: stop
	rm -rf box-* packer_cache/ output/
