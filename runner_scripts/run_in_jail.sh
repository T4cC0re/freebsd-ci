#!/usr/bin/env bash

RELEASE="$1"
shift
POTNAME="jailrunner_$(cat /dev/urandom | env LC_CTYPE=C tr -dc a-zA-Z0-9 | head -c 16)"
BASE="gitlab-ci-${RELEASE//./_}"

cleanup () {
  sudo pot destroy -vp ${POTNAME} -F
  exit $RC
}

trap cleanup EXIT SIGINT

RC=129
sudo pot clone -vp ${POTNAME} -P "$BASE"
echo "executing pot with '$*'"
sudo pot set-cmd -p "${POTNAME}" -c "$*"
sudo pot start -v ${POTNAME}
RC=$?
echo "pot quit with code ${RC}"
