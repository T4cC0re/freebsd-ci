#!/usr/bin/env bash
set -xe

EXECUTOR_VERSION="$(git describe --tags --dirty=-DEV --always)"
SSHOPTS="-o PreferredAuthentications=keyboard-interactive -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

ssh () {
  sshpass -pgitlab-ci ssh ${SSHOPTS} gitlab-ci@localhost -p5555 "${@}"
}

scp () {
  sshpass -pgitlab-ci scp ${SSHOPTS} -P5555 "${@}" "gitlab-ci@localhost:"
}

scp_exec () {
  sshpass -pgitlab-ci scp ${SSHOPTS} -P5555 "${@}" "gitlab-ci@localhost:/usr/local/lib/gitab-ci/"
}

prepare_base () {
  RELEASE="$1"

  ssh <<EOF
  set -xe
  sudo pot create-base -vr "${RELEASE}" || exit 0
EOF
}

prepare_ci () {
  RELEASE="$1"

  ssh <<EOF
  set -xe
  sudo pot create -p "gitlab-ci-${RELEASE//./_}" -b "${RELEASE}" || (sudo pot ls | grep "gitlab-ci-${RELEASE//./_}")
  sudo pot copy-in -p "gitlab-ci-${RELEASE//./_}" -s install_deps.sh -d /opt/custom/root/
  sudo pot copy-in -p "gitlab-ci-${RELEASE//./_}" -s /usr/local/etc/host-env -d /usr/local/etc/
  sudo pot run -f "gitlab-ci-${RELEASE//./_}" <<< 'freebsd-update --not-running-from-cron fetch && freebsd-update --not-running-from-cron install || true'
  sudo pot run -f "gitlab-ci-${RELEASE//./_}" <<< 'sh -c "ASSUME_ALWAYS_YES=yes pkg install -y curl sudo bash; sh install_deps.sh"'
  sudo pot stop "gitlab-ci-${RELEASE//./_}"
  sudo pot snap -vp "gitlab-ci-${RELEASE//./_}"
  sudo pot purge-snapshots -vp "gitlab-ci-${RELEASE//./_}"
EOF
}

echo "Copying files..."
scp runner_scripts/*.sh install_deps.sh
ssh <<EOF
  set -xe
  sudo mkdir -p /usr/local/lib/gitab-ci
  sudo chmod -R 775 /usr/local/lib/gitab-ci
  sudo chown -R gitlab-ci:gitlab-ci /usr/local/lib/gitab-ci
EOF
scp_exec runner_scripts/executor/*
ssh <<EOF
 sudo chmod -R 775 /usr/local/lib/gitab-ci
 sudo hostname "FreeBSD-CI-$(hostname)"
 echo "export POT_VERSION="\$(sudo pot version -q)"" | sudo tee /usr/local/etc/host-env
 echo "export EXECUTOR_VERSION="${EXECUTOR_VERSION}"" | sudo tee -a /usr/local/etc/host-env
 echo "export HOSTNAME="\$(hostname)"" | sudo tee -a /usr/local/etc/host-env
EOF

TAGS=""

echo "Preparing base pots..."
for BASE in ${@}; do
  echo "Preparing base pot for ${BASE}..."
  prepare_base "$BASE"
  prepare_ci "$BASE"
  TAGS="${TAGS},FreeBSD-${BASE}"
done

ssh sudo pot ls -v
if [ -n "${REG_TOKEN}" ]; then
    ssh <<EOF
      if [ "\$(cat /usr/local/etc/reg_token_hash)" == "\$(sha512 -qs "${REG_TOKEN}")" ]; then
        echo 'runner registered'
      else
        sudo -iu gitlab-runner gitlab-runner unregister --all-runners
        sudo -iu gitlab-runner gitlab-runner register \
        --name "\$(hostname)" \
        --tag-list "FreeBSD,amd64,${TAGS}" \
        --registration-token "${REG_TOKEN}" \
        --non-interactive \
        --executor custom \
        --url "https://gitlab.com/" \
        --custom-config-exec /usr/local/lib/gitab-ci/config_exec \
        --custom-prepare-exec /usr/local/lib/gitab-ci/prepare_exec \
        --custom-run-exec /usr/local/lib/gitab-ci/run_exec \
        --custom-cleanup-exec /usr/local/lib/gitab-ci/cleanup_exec
        sha512 -qs "${REG_TOKEN}" | sudo tee /usr/local/etc/reg_token_hash
      fi
EOF
fi

echo "Done"
